# Rust CoreUtils

This repository contains a Rust reimplementation of the coreutils. The focus is on having
simple, easy to understand code, but feature parity with the GNU implementations.

To make it easy to distribute these utilities, they can be built either separately or as a multicall
binary, similar to how busybox works.

Documentation is available [here](https://rustutils.gitlab.io/coreutils/doc/rustutils_core/index.html), built
from the main branch of this repository.

## Utilities

| Name | Description | Status |
| :--: | :-- | :--: |
| `arch` | Print machine architecture | Implemented |
| `cat` | Concatenate files to standard output | Implemented |
| `false` | Exits with a status code indicating failure | Implemented |
| `rmdir` | Remove empty directories | Implemented |
| `seq` | ||
| `sleep` | Pause for a specified amount of time | Implemented |
| `sync` | Synchronizes write buffers to permanent storage | Implemented |
| `tee` | Copy standard input to each FILE, and also to standard output | Implemented |
| `true` | Exits with a status code indicating success | Implemented |
| `unlink` | Remove files by calling the unlink function | Implemented |
| `wc` | Counts bytes, characters, words and lines | In Progress |
| `yes` | Repeatedly output a line with the specified strings, or 'y' | Implemented |

## Download

There are builds available for download.

| Platform | Architecture | Interface | Link |
| :--: | :--: | :--: | :--: |
| Linux | amd64 | gnu | [rustutils-core-linux-amd64][]|
| Linux | amd64 | musl | [rustutils-core-linux-musl-amd64][] |
| Linux | arm64 | gnu | [rustutils-core-linux-arm64][] |
| Linux | arm32 | gnu | [rustutils-core-linux-arm32][] |
| Linux | riscv64 | gnu | [rustutils-core-linux-riscv64][] |

[rustutils-core-linux-amd64]: https://rustutils.gitlab.io/coreutils/release/x86_64-unknown-linux-gnu/rustutils-core
[rustutils-core-linux-musl-amd64]: https://rustutils.gitlab.io/coreutils/release/x86_64-unknown-linux-musl/rustutils-core
[rustutils-core-linux-arm64]: https://rustutils.gitlab.io/coreutils/rustutils-core-linux-arm64
[rustutils-core-linux-arm32]: https://rustutils.gitlab.io/coreutils/rustutils-core-linux-arm32
[rustutils-core-linux-riscv64]: https://rustutils.gitlab.io/coreutils/rustutils-core-linux-riscv64

## Prior Art

- [BSD Coreutils](https://github.com/DiegoMagdaleno/BSDCoreUtils)
- [GNU Coreutils](https://www.gnu.org/software/coreutils/)
