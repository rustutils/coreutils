use std::error::Error;
use std::process::ExitCode;

pub trait Runnable {
    /// Run something, can either succeed or fail.
    fn run(&self) -> Result<(), Box<dyn Error>>;

    /// Run somethin, returning an ExitCode to return to the OS.
    fn main(&self) -> ExitCode {
        match self.run() {
            Ok(()) => ExitCode::SUCCESS,
            Err(error) => {
                eprintln!("Error: {error}");
                ExitCode::FAILURE
            }
        }
    }
}
