/// Import and re-export utility, if the feature for it is enabled.
macro_rules! import_utility {
    ($feature:literal => $module:path) => {
        #[cfg(feature = $feature)]
        pub use $module;
    };
}

/// By using some macro magic, this will import all of the parsers
/// publically, and declare a `Coreutil` enum that has cases for every
/// single utility listed here, and define some helpful functions that
/// enable the rest of this code.
macro_rules! declare_utility {
    ($($feature:literal => $module:path as $name:ident),*) => {
        $(crate::macros::import_utility!($feature => $module);)*
        crate::macros::define_coreutil!($($feature => $module as $name),*);
        crate::macros::impl_coreutil!($($feature => $module as $name),*);
    }
}

macro_rules! define_coreutil {
    ($($feature:literal => $module:path as $name:ident),*) => {
        #[derive(Parser, Clone, Debug)]
        #[allow(missing_docs)]
        /// Utility to run
        pub enum Coreutil {
            $(
                #[cfg(feature = $feature)]
                $name($module)
            ),*
        }
    }
}

macro_rules! impl_coreutil {
    ($($feature:literal => $module:path as $name:ident),*) => {
        impl Coreutil {
            /// Get a runnable from a Coreutil case
            fn runnable(&self) -> &dyn Runnable {
                use Coreutil::*;
                match self {
                    $(
                        #[cfg(feature = $feature)]
                        $name(args) => args
                    ),*
                }
            }

            /// Run the parser for a given binary
            fn parse(binary: &str) -> Option<Self> {
                use Coreutil::*;
                match binary {
                    $(
                        $feature => Some($name(<$module>::parse())),
                    )*
                    _ => None,
                }
            }
        }
    }
}

pub(crate) use declare_utility;
pub(crate) use define_coreutil;
pub(crate) use impl_coreutil;
pub(crate) use import_utility;
