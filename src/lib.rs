//! Multi-call binary for modern coreutils implemented in Rust, with a focus on
//! simplicity.
#![deny(warnings)]
#![deny(unsafe_code)]
#![deny(missing_docs)]

use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;
use std::process::ExitCode;
mod macros;

// Generate the `Coreutil` enum, which has cases for every utility compiled in,
// as well as some helper methods used for pasing arguments when run as a multi-call
// binary.
macros::declare_utility! {
    "arch" => rustutils_arch::Arch as Arch,
    "base64" => rustutils_base64::Base64 as Base64,
    "basename" => rustutils_basename::Basename as Basename,
    "cat" => rustutils_cat::Cat as Cat,
    "chroot" => rustutils_chroot::Chroot as Chroot,
    "dirname" => rustutils_dirname::Dirname as Dirname,
    "env" => rustutils_env::Env as Env,
    "factor" => rustutils_factor::Factor as Factor,
    "false" => rustutils_false::False as False,
    "mkdir" => rustutils_mkdir::Mkdir as Mkdir,
    "printenv" => rustutils_printenv::Printenv as Printenv,
    "pwd" => rustutils_pwd::Pwd as Pwd,
    "rmdir" => rustutils_rmdir::Rmdir as Rmdir,
    "seq" => rustutils_seq::Seq as Seq,
    "sleep" => rustutils_sleep::Sleep as Sleep,
    "tee" => rustutils_tee::Tee as Tee,
    "true" => rustutils_true::True as True,
    "uname" => rustutils_uname::Uname as Uname,
    "unlink" => rustutils_unlink::Unlink as Unlink,
    "wc" => rustutils_wc::Wc as Wc,
    "yes" => rustutils_yes::Yes as Yes
}

/// Command-line options for the non-multicall invocation of this binary, where every
/// utility is available as a subcommand. This is used as the fallback.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Coreutils {
    #[clap(subcommand)]
    util: Coreutil,
}

impl Runnable for Coreutils {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        self.util.runnable().run()
    }

    fn main(&self) -> ExitCode {
        self.util.runnable().main()
    }
}

/// Check if this binary is being called as another utility, and if so, return
/// the parsed command-line options of that utility.
pub fn multicall_parse() -> Result<Option<Coreutil>, Box<dyn Error>> {
    let executable = std::env::current_exe()?;
    let file = executable.file_stem().and_then(|s| s.to_str());
    let result = file.and_then(|name| Coreutil::parse(name));
    Ok(result)
}

/// Run the coreutils implementation.
///
/// When the `multicall` feature is enabled, this will attempt to look at the name of
/// the executable file that this was launched as, and run the corresponding utility.
pub fn main() -> ExitCode {
    if cfg!(feature = "multicall") {
        match multicall_parse() {
            Ok(Some(utility)) => utility.runnable().main(),
            Ok(None) => Coreutils::parse().main(),
            Err(error) => {
                eprintln!("Error parsing multicall: {error}");
                ExitCode::FAILURE
            }
        }
    } else {
        Coreutils::parse().main()
    }
}
