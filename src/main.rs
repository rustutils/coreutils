use std::process::ExitCode;

fn main() -> ExitCode {
    rustutils_core::main()
}
