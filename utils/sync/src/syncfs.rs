use std::error::Error;
use std::fs::File;
#[cfg(unix)]
use std::os::unix::io::AsRawFd;

pub trait SyncFilesystem {
    fn sync_filesystem(&self) -> Result<(), Box<dyn Error>>;
}

impl SyncFilesystem for File {
    #[cfg(unix)]
    fn sync_filesystem(&self) -> Result<(), Box<dyn Error>> {
        let fd = self.as_raw_fd();
        unsafe {
            if nix::libc::syncfs(fd) != 0 {
                unimplemented!()
            }
        }
        Ok(())
    }

    #[cfg(target_arch = "wasm32")]
    fn sync_filesystem(&self) -> Result<(), Box<dyn Error>> {
        Err(String::from("syncing the filesystem is unsupported for wasm32").into())
    }
}
