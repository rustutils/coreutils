use clap::Parser;
use rustutils_runnable::Runnable;
use rustutils_sync::Sync;
use std::process::ExitCode;

fn main() -> ExitCode {
    Sync::parse().main()
}
