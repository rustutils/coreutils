use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;
use std::fs::File;
use std::path::PathBuf;
pub use syncfs::SyncFilesystem;

mod syncfs;

/// Synchronize cached writes to persistent storage.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Sync {
    /// Sync only the file data, no metadata
    #[clap(short, long)]
    data: bool,
    /// Sync the entire filesystem that contains the data.
    #[clap(short, long, conflicts_with = "data")]
    file_system: bool,
    /// List of files to synchronize
    files: Vec<PathBuf>,
}

impl Runnable for Sync {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        if self.files.len() == 0 {
            #[cfg(unix)]
            nix::unistd::sync();
            #[cfg(target_arch = "wasm32")]
            return Err(String::from("syncing the filesystem is unsupported for wasm32").into());
        } else {
            for file in &self.files {
                let file = File::open(file)?;
                if self.file_system {
                    file.sync_filesystem()?;
                } else if self.data {
                    file.sync_data()?;
                } else {
                    file.sync_all()?;
                }
            }
        }
        Ok(())
    }
}
