use clap::Parser;
use rustutils_dirname::Dirname;
use rustutils_runnable::Runnable;
use std::process::ExitCode;

fn main() -> ExitCode {
    Dirname::parse().main()
}
