use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;
use std::io::Write;
use std::os::unix::ffi::OsStrExt;
use std::path::{Path, PathBuf};

/// Output the parent directory of each path.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Dirname {
    #[clap(required = true)]
    path: Vec<PathBuf>,
    /// End each output line with a NUL character instead of a newline.
    #[clap(short, long)]
    zero: bool,
}

/// Given a path, compue the parent directory path.
///
/// If the path is relative, it assumes that it is relative to the current directory
/// (`.`). When the root path (`/`) is given, it returns the root path (`/`).
pub fn dirname(path: &Path) -> &Path {
    match path.parent() {
        Some(path) if path.as_os_str().len() == 0 => Path::new("."),
        Some(path) => path,
        None if path.as_os_str().len() == 0 => Path::new("."),
        None => Path::new("/"),
    }
}

impl Runnable for Dirname {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        // Since Linux paths do not have to be valid UTF-8, but Rust strings do, we need to
        // be careful here not to use println or similar macros, but rather write directly
        // to standard output.
        let mut stdout = std::io::stdout();
        for path in &self.path {
            stdout.write_all(dirname(&path).as_os_str().as_bytes())?;
            if self.zero {
                stdout.write_all(&[0])?;
            } else {
                stdout.write_all(&[b'\n'])?;
            }
        }

        Ok(())
    }
}

#[test]
fn dirname_absolute_works() {
    assert_eq!(dirname(&Path::new("/")), Path::new("/"));
    assert_eq!(dirname(&Path::new("/abc")), Path::new("/"));
    assert_eq!(dirname(&Path::new("/abc/")), Path::new("/"));
    assert_eq!(dirname(&Path::new("/abc/def")), Path::new("/abc"));
}

#[test]
fn dirname_relative_works() {
    assert_eq!(dirname(&Path::new("")), Path::new("."));
    assert_eq!(dirname(&Path::new(".")), Path::new("."));
    assert_eq!(dirname(&Path::new("abc")), Path::new("."));
    assert_eq!(dirname(&Path::new("./abc")), Path::new("."));
    assert_eq!(dirname(&Path::new("./abc/")), Path::new("."));
    assert_eq!(dirname(&Path::new("./abc/def")), Path::new("./abc"));
    assert_eq!(dirname(&Path::new("../abc")), Path::new(".."));
    assert_eq!(dirname(&Path::new("../abc/")), Path::new(".."));
    assert_eq!(dirname(&Path::new("../../abc")), Path::new("../.."));
}
