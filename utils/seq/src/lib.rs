use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;

/// Print numbers from FIRST to LAST, in steps of INCREMENT.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Seq {
    /// Use printf-style floating-point format
    #[clap(short, long)]
    format: Option<String>,
    /// Use separator to separate numbers.
    #[clap(short, long, default_value = "\n")]
    separator: String,
    /// Equalize width by padding with leading zeroes
    #[clap(short, long)]
    equal_width: bool,

    /// First
    #[clap(default_value = "1")]
    start: Number,
    /// Increment
    #[clap(default_value = "1")]
    increment: Number,
    /// End
    last: Option<Number>,
}

#[derive(Clone, Debug)]
pub enum Number {
    Float(f64),
    Integer(i64),
}

impl std::str::FromStr for Number {
    type Err = Box<dyn Error + Send + Sync + 'static>;
    fn from_str(value: &str) -> Result<Self, Self::Err> {
        let number: i64 = value.parse()?;
        Ok(Number::Integer(number))
    }
}

impl Runnable for Seq {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        println!("{self:?}");
        Ok(())
    }
}
