use clap::Parser;
use rustutils_runnable::Runnable;
use rustutils_seq::Seq;
use std::process::ExitCode;

fn main() -> ExitCode {
    Seq::parse().main()
}
