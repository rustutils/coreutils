use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;

/// Print machine architecture.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Arch {}

impl Runnable for Arch {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        let uname = nix::sys::utsname::uname()?;
        println!("{}", uname.machine().to_string_lossy());
        Ok(())
    }
}
