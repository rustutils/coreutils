use clap::Parser;
use rustutils_arch::Arch;
use rustutils_runnable::Runnable;
use std::process::ExitCode;

fn main() -> ExitCode {
    Arch::parse().main()
}
