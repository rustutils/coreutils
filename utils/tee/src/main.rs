use clap::Parser;
use rustutils_runnable::Runnable;
use rustutils_tee::Tee;
use std::process::ExitCode;

fn main() -> ExitCode {
    Tee::parse().main()
}
