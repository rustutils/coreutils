use clap::Parser;
use rustutils_runnable::Runnable;
use rustutils_wc::Wc;
use std::process::ExitCode;

fn main() -> ExitCode {
    Wc::parse().main()
}
