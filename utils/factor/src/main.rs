use clap::Parser;
use rustutils_factor::Factor;
use rustutils_runnable::Runnable;
use std::process::ExitCode;

fn main() -> ExitCode {
    Factor::parse().main()
}
