use clap::Parser;
use rustutils_runnable::Runnable;
use rustutils_unlink::Unlink;
use std::process::ExitCode;

fn main() -> ExitCode {
    Unlink::parse().main()
}
