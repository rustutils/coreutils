use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;
use std::fs::remove_file;
use std::io;
use std::path::PathBuf;

/// Remove files by calling the unlink function.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about)]
pub struct Unlink {
    /// List of files to remove.
    #[clap(required = true)]
    pub files: Vec<PathBuf>,
}

impl Unlink {
    /// Iterate through supplied files and remove them.
    pub fn run(&self) -> Result<(), io::Error> {
        for file in &self.files {
            remove_file(&file)?;
        }
        Ok(())
    }
}

impl Runnable for Unlink {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        self.run().map_err(|e| Box::new(e) as Box<dyn Error>)
    }
}
