use clap::Parser;
use rustutils_pwd::Pwd;
use rustutils_runnable::Runnable;
use std::process::ExitCode;

fn main() -> ExitCode {
    Pwd::parse().main()
}
