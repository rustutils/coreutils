use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;

/// Print the full path of the current working directory.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Pwd {
    /// Print the path, even if it contains symbolic links.
    #[clap(short = 'L', long)]
    logical: bool,
    /// Resolve the path to the absolute path, without any symbolic links.
    #[clap(short = 'P', long)]
    physical: bool,
}

impl Runnable for Pwd {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        // normalize by default
        let normalize = match (self.logical, self.physical) {
            (true, false) => false,
            _ => true,
        };
        let dir = std::env::current_dir()?;
        let dir = if normalize { dir.canonicalize()? } else { dir };
        println!("{}", dir.display());
        Ok(())
    }
}
