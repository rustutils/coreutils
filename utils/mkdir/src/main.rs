use clap::Parser;
use rustutils_mkdir::Mkdir;
use rustutils_runnable::Runnable;
use std::process::ExitCode;

fn main() -> ExitCode {
    Mkdir::parse().main()
}
