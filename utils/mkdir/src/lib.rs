use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;
use std::fs::create_dir;
use std::io;
use std::path::{Path, PathBuf};

/// Create directories, if they do not already exist.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about)]
pub struct Mkdir {
    /// Remove directory and its ancestors.
    #[clap(long, short)]
    pub parents: bool,
    /// Output a diagnostic every time a directory is removed.
    #[clap(long, short)]
    pub verbose: bool,
    /// List of directories to remove.
    #[clap(required = true)]
    pub directory: Vec<PathBuf>,
}

impl Mkdir {
    /// Run command
    pub fn run(&self) -> Result<(), io::Error> {
        for directory in &self.directory {
            if self.parents {
                self.create_parents(directory)?
            } else {
                self.create_directory(directory)?
            }
        }
        Ok(())
    }

    /// Create all parents and the directory
    pub fn create_parents(&self, path: &Path) -> Result<(), io::Error> {
        if let Some(parent) = path.parent() {
            self.create_parents(parent)?;
        }
        self.create_directory(path)?;
        Ok(())
    }

    /// Create directory
    pub fn create_directory(&self, dir: &Path) -> Result<(), io::Error> {
        if self.verbose {
            eprintln!("Mkdir: Creating directory {dir:?}");
        }

        create_dir(dir)?;
        Ok(())
    }
}

impl Runnable for Mkdir {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        self.run().map_err(|e| Box::new(e) as Box<dyn Error>)
    }
}
