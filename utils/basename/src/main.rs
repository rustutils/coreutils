use clap::Parser;
use rustutils_basename::Basename;
use rustutils_runnable::Runnable;
use std::process::ExitCode;

fn main() -> ExitCode {
    Basename::parse().main()
}
