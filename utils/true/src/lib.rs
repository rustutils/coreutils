use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;

/// Exit with a status code indicating success.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct True {}

impl Runnable for True {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        Ok(())
    }
}

#[test]
fn can_run_successfully() {
    let cmd = True {};
    assert!(cmd.run().is_ok());
}
