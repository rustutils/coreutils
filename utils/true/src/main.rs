use clap::Parser;
use rustutils_runnable::Runnable;
use rustutils_true::True;
use std::process::ExitCode;

fn main() -> ExitCode {
    True::parse().main()
}
