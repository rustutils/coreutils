use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;
use std::thread::sleep;
use std::time::Duration;

/// Pause for a specified amount of time.
///
/// The argument is assumed to be in seconds, however a suffix may be used to
/// indicate otherwise. Use `h` to pause for a specified amount of hours, `m`
/// for minutes, or `d` for days.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Sleep {
    /// Time to sleep for
    #[clap(parse(try_from_str = parse_duration))]
    time: Duration,
}

pub fn parse_duration(value: &str) -> Result<Duration, humantime::DurationError> {
    value
        .parse()
        .map(|seconds: u64| Duration::from_secs(seconds))
        .or_else(|_| humantime::parse_duration(value))
}

impl Runnable for Sleep {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        sleep(self.time);
        Ok(())
    }
}
