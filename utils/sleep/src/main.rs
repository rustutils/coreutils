use clap::Parser;
use rustutils_runnable::Runnable;
use rustutils_sleep::Sleep;
use std::process::ExitCode;

fn main() -> ExitCode {
    Sleep::parse().main()
}
