use clap::Parser;
use rustutils_runnable::Runnable;
use rustutils_uname::Uname;
use std::process::ExitCode;

fn main() -> ExitCode {
    Uname::parse().main()
}
