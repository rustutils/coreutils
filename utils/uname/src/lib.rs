use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;
use std::io::Write;
use std::os::unix::ffi::OsStrExt;

/// Print system information.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Uname {
    /// Print all information
    #[clap(short, long)]
    all: bool,
    /// Print the kernel name
    #[clap(short = 's', long)]
    kernel_name: bool,
    /// Print the network node hostname
    #[clap(short, long, alias = "nodename")]
    node_name: bool,
    /// Print the kernel release
    #[clap(short = 'r', long)]
    kernel_release: bool,
    /// Print the kernel version
    #[clap(short = 'v', long)]
    kernel_version: bool,
    /// Print the machine hardware name
    #[clap(short, long)]
    machine: bool,
    /// Print the processor type
    #[clap(short, long)]
    processor: bool,
    /// Print the hardware platform
    #[clap(short = 'i', long)]
    hardware_platform: bool,
    /// Print the operating system
    #[clap(short, long)]
    operating_system: bool,
    /// Format the output as JSON
    #[clap(short, long)]
    json: bool,
}

impl Runnable for Uname {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        let uname = nix::sys::utsname::uname()?;
        let mut output = vec![];
        if self.kernel_name || self.all {
            output.push(uname.sysname());
        }

        if self.node_name || self.all {
            output.push(uname.nodename());
        }

        if self.kernel_release || self.all {
            output.push(uname.release());
        }

        if self.kernel_version || self.all {
            output.push(uname.version());
        }

        if self.machine || self.all {
            output.push(uname.machine());
        }

        let mut stdout = std::io::stdout();
        for string in &output {
            stdout.write_all(b" ")?;
            stdout.write_all(string.as_bytes())?;
        }
        stdout.write_all(b"\n")?;

        Ok(())
    }
}
