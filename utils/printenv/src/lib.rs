use clap::Parser;
use rustutils_runnable::Runnable;
use std::env;
use std::error::Error;
use std::ffi::{OsStr, OsString};
use std::io::Write;
use std::os::unix::ffi::OsStrExt;

/// Print the values of the specified environment variables.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Printenv {
    /// When printing the current environment, separate the variables with a NUL character.
    #[clap(short = '0', long)]
    null: bool,
    /// When printing the current environment, output it as JSON.
    #[clap(short, long, conflicts_with = "null")]
    json: bool,
    /// Environment variables to print.
    variables: Vec<OsString>,
}

impl Printenv {
    pub fn run(&self) -> Result<(), Box<dyn Error>> {
        let separator = match self.null {
            true => OsStr::new("\0"),
            false => OsStr::new("\n"),
        };
        self.print_variables(separator)
    }

    pub fn print_variables(&self, separator: &OsStr) -> Result<(), Box<dyn Error>> {
        let mut stdout = std::io::stdout();
        for name in &self.variables {
            let value =
                env::var_os(name).ok_or_else(|| format!("Missing env variable {name:?}"))?;
            stdout.write_all(value.as_bytes())?;
            stdout.write_all(separator.as_bytes())?;
        }
        Ok(())
    }
}

impl Runnable for Printenv {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        self.run()?;
        Ok(())
    }
}
