use clap::Parser;
use rustutils_printenv::Printenv;
use rustutils_runnable::Runnable;
use std::process::ExitCode;

fn main() -> ExitCode {
    Printenv::parse().main()
}
