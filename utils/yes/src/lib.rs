use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;
use std::io::{stdout, BufWriter, Write};

/// Repeatedly output a line with the specified strings, or 'y'.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Yes {
    /// String to repeatedly output
    #[clap(default_value = "y")]
    string: Vec<String>,
}

impl Runnable for Yes {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        let mut string = self.string.join(" ");
        string.push('\n');
        let mut stdout = BufWriter::new(stdout());
        loop {
            stdout.write_all(&string.as_bytes())?;
        }
    }
}
