use clap::Parser;
use rustutils_runnable::Runnable;
use rustutils_yes::Yes;
use std::process::ExitCode;

fn main() -> ExitCode {
    Yes::parse().main()
}
