use clap::Parser;
use rustutils_rmdir::Rmdir;
use rustutils_runnable::Runnable;
use std::process::ExitCode;

fn main() -> ExitCode {
    Rmdir::parse().main()
}
