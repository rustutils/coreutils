use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;
use std::fs::remove_dir;
use std::io;
use std::path::{Path, PathBuf};

/// Remove directories, if they are empty.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about)]
pub struct Rmdir {
    /// Remove directory and its ancestors.
    #[clap(long, short)]
    pub parents: bool,
    /// Ignore failures that are solely because a directory is non-empty.
    #[clap(long, short)]
    pub ignore_fail_on_non_empty: bool,
    /// Output a diagnostic every time a directory is removed.
    #[clap(long, short)]
    pub verbose: bool,
    /// List of directories to remove.
    #[clap(required = true)]
    pub directories: Vec<PathBuf>,
}

impl Rmdir {
    /// Run command
    pub fn run(&self) -> Result<(), io::Error> {
        for directory in &self.directories {
            if self.parents {
                self.remove_parents(directory)?
            } else {
                self.remove_directory(directory)?
            }
        }
        Ok(())
    }

    /// Remove all parents and the directory
    pub fn remove_parents(&self, path: &Path) -> Result<(), io::Error> {
        for ancestor in path.ancestors() {
            if ancestor.parent().is_some() {
                self.remove_directory(ancestor)?;
            }
        }
        Ok(())
    }

    /// Remove directory
    pub fn remove_directory(&self, dir: &Path) -> Result<(), io::Error> {
        if self.verbose {
            eprintln!("rmdir: removing directory, {dir:?}");
        }

        match remove_dir(dir) {
            Ok(()) => Ok(()),
            // Match on io::ErrorKind::DirectoryNotEmpty once library features 'io_error_more'
            // stabilises. This is a hack for now, it is not pretty but it works.
            Err(error)
                if self.ignore_fail_on_non_empty
                    && error.kind().to_string() == "directory not empty" =>
            {
                if self.verbose {
                    eprintln!("rmdir: ignoring error, '{error}'");
                }
                Ok(())
            }
            Err(error) => Err(error),
        }
    }
}

impl Runnable for Rmdir {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        self.run().map_err(|e| Box::new(e) as Box<dyn Error>)
    }
}
