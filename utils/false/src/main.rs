use clap::Parser;
use rustutils_false::False;
use rustutils_runnable::Runnable;
use std::process::ExitCode;

fn main() -> ExitCode {
    False::parse().main()
}
