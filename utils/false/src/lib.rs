use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;
use std::process::ExitCode;

/// Exit with a status code indicating failure.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct False {
    #[clap(long, short)]
    code: Option<u8>,
}

impl Runnable for False {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        Ok(())
    }

    fn main(&self) -> ExitCode {
        match self.code {
            None => ExitCode::FAILURE,
            Some(code) => ExitCode::from(code),
        }
    }
}
