use clap::Parser;
use rustutils_runnable::Runnable;
use rustutils_chroot::Chroot;
use std::process::ExitCode;

fn main() -> ExitCode {
    Chroot::parse().main()
}
