use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;
use std::ffi::OsString;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::os::unix::process::CommandExt;

pub const DEFAULT_SHELL: &str = "/bin/sh";

/// Run command with root directory set to a different directory.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Chroot {
    /// Specify supplementary groups.
    #[clap(long)]
    groups: Vec<String>,
    /// Specify user and group to use.
    #[clap(long)]
    userspec: Option<UserSpec>,
    /// Do not change working directory to `/`.
    #[clap(long)]
    skip_chdir: bool,
    /// New root directory to run command in.
    newroot: PathBuf,
    /// Command to run.
    ///
    /// If no command is given, runs `$SHELL -i`.
    #[clap(last = true)]
    command: Vec<OsString>,
}

#[derive(Debug, Clone)]
pub struct UserSpec {
    pub user: String,
    pub group: String,
}

impl std::str::FromStr for UserSpec {
    type Err = String;
    fn from_str(spec: &str) -> Result<Self, Self::Err> {
        let mut parts = spec.split(":");
        let user = parts.next().ok_or("Missing user".to_string())?.to_string();
        let group = parts.next().ok_or("Missing group".to_string())?.to_string();
        Ok(UserSpec {
            user,
            group,
        })
    }
}

impl Chroot {
    pub fn run(&self) -> Result<(), Box<dyn Error>> {
        if !self.groups.is_empty() || self.userspec.is_some() {
            unimplemented!()
        }

        self.change_root()?;

        if !self.skip_chdir {
            self.change_directory()?;
        }

        let mut command = self.command();
        command.exec();

        Ok(())
    }

    /// Change root directory.
    fn change_root(&self) -> Result<(), Box<dyn Error>> {
        nix::unistd::chroot(&self.newroot)?;
        Ok(())
    }

    /// Change current directory to the new root.
    fn change_directory(&self) -> Result<(), Box<dyn Error>> {
        std::env::set_current_dir(&Path::new("/"))?;
        Ok(())
    }

    /// Determine command to run.
    pub fn command(&self) -> Command {
        if self.command.is_empty() {
            let shell = std::env::var_os("SHELL")
                .unwrap_or_else(|| DEFAULT_SHELL.into());
            let mut command = Command::new(shell);
            command.arg("-i");
            command
        } else {
            let mut command = Command::new(&self.command[0]);
            command.args(self.command.iter().skip(1));
            command
        }
    }
}

impl Runnable for Chroot {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        self.run()?;
        Ok(())
    }
}
