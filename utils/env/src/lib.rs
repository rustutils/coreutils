use clap::Parser;
use rustutils_runnable::Runnable;
use std::env;
use std::error::Error;
use std::ffi::{OsStr, OsString};
use std::io::Write;
use std::os::unix::ffi::OsStrExt;
use std::process::{Command, ExitStatus};

/// Set environment variables and run command.
///
/// When no command is supplied, print the current environment variables.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Env {
    /// Start with an empty environment.
    #[clap(short, long)]
    ignore_environment: bool,
    /// When printing the current environment, separate the variables with a NUL character.
    #[clap(short = '0', long)]
    null: bool,
    /// When printing the current environment, output it as JSON.
    #[clap(short, long, conflicts_with = "null")]
    json: bool,
    /// Unset variables.
    #[clap(short, long)]
    unset: Vec<OsString>,
    /// Environment variables to set, in the shape of `NAME=value`.
    variables: Vec<OsString>,
    /// Command to run, using the specified environment.
    #[clap(last = true)]
    command: Vec<OsString>,
}

/// Parse an environment variable definition.
///
/// Environment variables are specified in the form `VARIABLE=value`. Typically, the variables are
/// uppercased, but this is not mandatory.
pub fn parse_variable(var: &OsStr) -> Option<(&OsStr, &OsStr)> {
    let var = var.as_bytes();
    var.iter()
        .enumerate()
        .skip(1)
        .filter(|(_index, byte)| **byte == b'=')
        .map(|(index, _byte)| index)
        .last()
        .map(|pos| {
            (
                OsStr::from_bytes(&var[..pos]),
                OsStr::from_bytes(&var[pos + 1..]),
            )
        })
}

#[test]
fn can_parse_variable() {
    assert_eq!(parse_variable(OsStr::from_bytes(b"")), None);
    assert_eq!(parse_variable(OsStr::from_bytes(b"=")), None);
    assert_eq!(parse_variable(OsStr::from_bytes(b"VAR")), None);
    assert_eq!(parse_variable(OsStr::from_bytes(b"=VAR")), None);
    assert_eq!(
        parse_variable(OsStr::from_bytes(b"VAR=value")),
        Some((OsStr::from_bytes(b"VAR"), OsStr::from_bytes(b"value")))
    );
    assert_eq!(
        parse_variable(OsStr::from_bytes(b"VAR=")),
        Some((OsStr::from_bytes(b"VAR"), OsStr::from_bytes(b"")))
    );
}

impl Env {
    /// Setup environment
    pub fn setup(&self) {
        // unset every environment variable
        if self.ignore_environment {
            for (name, _) in env::vars_os() {
                env::remove_var(name);
            }
        }

        // unset request environment variables
        for variable in &self.unset {
            env::remove_var(variable);
        }
    }

    pub fn parse_arguments(&self) -> Result<usize, Box<dyn Error>> {
        let count = self
            .variables
            .iter()
            .map(|variable| parse_variable(variable))
            .take_while(|parsed| parsed.is_some())
            .filter_map(|parsed| parsed)
            .inspect(|(name, value)| env::set_var(name, value))
            .count();
        Ok(count)
    }

    pub fn run_command(&self, offset: usize) -> Result<Option<ExitStatus>, Box<dyn Error>> {
        let mut args = self
            .variables
            .iter()
            .skip(offset)
            .chain(self.command.iter());
        if let Some(arg) = args.next() {
            let mut command = Command::new(arg);
            let result = command.args(args).spawn()?.wait()?;
            Ok(Some(result))
        } else {
            Ok(None)
        }
    }

    pub fn run(&self) -> Result<Option<ExitStatus>, Box<dyn Error>> {
        self.setup();
        let offset = self.parse_arguments()?;
        let result = self.run_command(offset)?;
        if result.is_none() {
            self.show()?;
        }
        Ok(result)
    }

    pub fn show(&self) -> Result<(), Box<dyn Error>> {
        if self.json {
            self.show_json()
        } else {
            let separator = match self.null {
                true => OsStr::new("\0"),
                false => OsStr::new("\n"),
            };
            self.show_normal(separator)
        }
    }

    pub fn show_json(&self) -> Result<(), Box<dyn Error>> {
        let mut environment: std::collections::BTreeMap<String, String> = Default::default();
        for (name, value) in env::vars_os() {
            if let (Some(name), Some(value)) = (name.to_str(), value.to_str()) {
                environment.insert(name.to_string(), value.to_string());
            }
        }
        let mut stdout = std::io::stdout();
        stdout.write_all(serde_json::to_string(&environment)?.as_bytes())?;
        Ok(())
    }
    pub fn show_normal(&self, separator: &OsStr) -> Result<(), Box<dyn Error>> {
        let mut stdout = std::io::stdout();
        for (name, value) in env::vars_os() {
            stdout.write_all(name.as_bytes())?;
            stdout.write_all(b"=")?;
            stdout.write_all(value.as_bytes())?;
            stdout.write_all(separator.as_bytes())?;
        }
        Ok(())
    }
}

impl Runnable for Env {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        self.run()?;
        Ok(())
    }
}
