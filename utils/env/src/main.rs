use clap::Parser;
use rustutils_env::Env;
use rustutils_runnable::Runnable;
use std::process::ExitCode;

fn main() -> ExitCode {
    Env::parse().main()
}
