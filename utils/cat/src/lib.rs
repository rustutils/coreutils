use clap::Parser;
use rustutils_runnable::Runnable;
use std::error::Error;
use std::fs::File;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};

pub const BUFFER_SIZE: usize = 4 * 1024;

/// Concatenate files to standard output.
#[derive(Parser, Clone, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Cat {
    /// List of files to concatenate.
    files: Vec<PathBuf>,
}

impl Cat {
    pub fn iter(&self) -> CatIterator {
        CatIterator::new(self.files.iter().map(|p| p.as_path()))
    }
}

#[derive(thiserror::Error, Debug)]
pub enum CatIteratorError {
    #[error("Opening file {0:?}: {1:}")]
    OpeningFile(PathBuf, std::io::Error),
    #[error("Reading from file {0:?}: {1:}")]
    ReadingFile(PathBuf, std::io::Error),
}

pub struct CatIterator<'f> {
    /// Current file to read from.
    current: Option<(&'f Path, Box<dyn Read>)>,
    /// List of files to read still.
    files: Box<dyn Iterator<Item = &'f Path> + 'f>,
}

impl<'f> CatIterator<'f> {
    pub fn new(files: impl Iterator<Item = &'f Path> + 'f) -> Self {
        CatIterator {
            current: None,
            files: Box::new(files),
        }
    }
}

impl<'f> Iterator for CatIterator<'f> {
    type Item = Result<Vec<u8>, CatIteratorError>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.current.is_none() {
            if let Some(next) = self.files.next() {
                let reader: Box<dyn Read> = if next.as_os_str() == "-" {
                    Box::new(std::io::stdin())
                } else {
                    match File::open(next) {
                        Err(error) => {
                            return Some(Err(CatIteratorError::OpeningFile(
                                next.to_path_buf(),
                                error,
                            )))
                        }
                        Ok(file) => Box::new(file),
                    }
                };
                self.current = Some((next, reader));
            } else {
                return None;
            }
        }
        let (next, reader) = match &mut self.current {
            Some((next, reader)) => (next, reader),
            None => unreachable!(),
        };
        let mut buffer = vec![0; BUFFER_SIZE];
        let length = match reader.read(&mut buffer[..]) {
            Err(error) => {
                return Some(Err(CatIteratorError::ReadingFile(
                    next.to_path_buf(),
                    error,
                )))
            }
            Ok(length) => length,
        };
        buffer.truncate(length);
        if length == 0 {
            self.current = None;
        }
        Some(Ok(buffer))
    }
}

impl Runnable for Cat {
    fn run(&self) -> Result<(), Box<dyn Error>> {
        let mut stdout = std::io::stdout();
        for data in self.iter() {
            stdout.write_all(&data?)?;
        }
        Ok(())
    }
}
