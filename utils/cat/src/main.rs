use clap::Parser;
use rustutils_cat::Cat;
use rustutils_runnable::Runnable;
use std::process::ExitCode;

fn main() -> ExitCode {
    Cat::parse().main()
}
