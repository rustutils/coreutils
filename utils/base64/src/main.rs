use clap::Parser;
use rustutils_base64::Base64;
use rustutils_runnable::Runnable;
use std::process::ExitCode;

fn main() -> ExitCode {
    Base64::parse().main()
}
